//
//  RequestType.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#ifndef RequestType_h
#define RequestType_h

typedef NS_ENUM(NSUInteger, RequestType) {
    RequestTypeGET,
};

#endif /* RequestType_h */
