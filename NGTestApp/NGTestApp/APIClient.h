//
//  APIClient.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@interface APIClient : NSObject

@property (nonatomic, strong) NSURL *baseURL;

- (RACSignal *)getCharacters;

@end
