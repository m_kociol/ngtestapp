//
//  CharacterDetail.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Mantle/Mantle.h>
#import <MTLJSONAdapter.h>
#import <MTLModel.h>
#import <UIKit/UIKit.h>

@interface CharacterDetail : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *characterDetailTitle;
@property (nonatomic, strong) NSString *characterDetailUrl;
@property (nonatomic, strong) NSString *characterDetailAbstract;
@property (nonatomic, strong) NSString *characterDetailThumbnail;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, assign) CGFloat characterHeight;

@end
