//
//  ViewModelAssembly.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class MainViewModel;
@class CoreComponentsAssembly;

@interface ViewModelAssembly : TyphoonAssembly

@property (nonatomic, strong) CoreComponentsAssembly *coreComponentsAssembly;

- (MainViewModel *)mainViewModel;

@end
