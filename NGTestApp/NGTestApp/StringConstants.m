//
//  StringConstants.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "StringConstants.h"

@implementation StringConstants

NSString *const MainCharactersTableViewCellNibName = @"MainCharactersTableViewCell";

@end
