//
//  MainCharactersTableViewCell.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCharactersTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *thumbnail;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *abstractLabel;
@property (nonatomic, strong) UIButton *favoriteButton;

+ (NSString *)reuseIdentifier;

@end
