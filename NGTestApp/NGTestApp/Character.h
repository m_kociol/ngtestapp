//
//  Character.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Mantle/Mantle.h>
#import <MTLJSONAdapter.h>
#import <MTLModel.h>
#import "CharacterDetail.h"

@interface Character : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *basePath;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSMutableArray<CharacterDetail *> *characterDetail;

@end
