//
//  MainViewController.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "MainViewController.h"
#import "MainViewModel.h"
#import "Masonry.h"
#import "MainCharactersTableViewCell.h"
#import "StringConstants.h"
#import "NumberConstants.h"
#import "Character.h"
#import "CharacterDetail.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDelegate, LongPressureDelegate>

@property (nonatomic, strong) UITableView *charactersTableView;
@property (nonatomic, assign) CGFloat expandedRowHeight;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar];
    self.viewModel.delegate = self;
    [self loadTableView];
    [self setupTableViewDelegates];
    [self registerTableViewClass];
    [self getCharacters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation setup

- (void)setupNavigationBar {
    self.title = NSLocalizedString(@"navigation.main.title", nil);
    UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter"] style:UIBarButtonItemStylePlain target:self action:@selector(filterButtonTouched:)];
    self.navigationItem.leftBarButtonItem = filterButton;
}

#pragma mark - TableViewCreator

- (void)loadTableView {
    self.charactersTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:self.charactersTableView];
    [self.charactersTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.view.mas_left);
        make.bottom.equalTo(self.view.mas_bottom);
        make.right.equalTo(self.view.mas_right);
    }];
    self.charactersTableView.hidden = YES;
}

#pragma mark - TableViewDelegates

- (void)setupTableViewDelegates {
    self.charactersTableView.dataSource = self.viewModel;
    self.charactersTableView.delegate = self;
    self.viewModel.tableView = self.charactersTableView;
}

#pragma mark - TableViewClassRegister

- (void)registerTableViewClass {
    [self.charactersTableView registerClass:[MainCharactersTableViewCell class] forCellReuseIdentifier:[MainCharactersTableViewCell reuseIdentifier]];
}

#pragma mark - TableView methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.viewModel.character.characterDetail[indexPath.row].characterHeight != 0.0) {
        return self.viewModel.character.characterDetail[indexPath.row].characterHeight;
    } else {
        return CharacterCellHeight;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailViewController *detailViewController = [[DetailViewController alloc] init];
    detailViewController.characterDetail = self.viewModel.character.characterDetail[indexPath.row];
    detailViewController.baseURL = self.baseURL;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - Data bindings

- (void)getCharacters {
    [[self.viewModel getCharacters] subscribeNext:^(id x) {
        NSLog(@"%@", x);
        self.charactersTableView.hidden = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.charactersTableView reloadData];
        });
    }];
}

#pragma mark - Private 

- (CGFloat)getLabelHeight:(UILabel*)label {
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (void)expandSelectedRowWithCell:(MainCharactersTableViewCell *)cell index:(NSInteger)index {
    CGFloat labelHeight = [self getLabelHeight:cell.abstractLabel];
    self.expandedRowHeight = labelHeight - AbstractHeight + CharacterCellHeight + CharacterCellExtendedOffset;
    
    if (self.viewModel.character.characterDetail[index].characterHeight != 0) {
        self.viewModel.character.characterDetail[index].characterHeight = 0.0f;
        [self.charactersTableView reloadData];
        return;
    }
    
    self.viewModel.character.characterDetail[index].characterHeight = self.expandedRowHeight;
    [self.charactersTableView reloadData];
}

#pragma mark - Buttons

- (void)filterButtonTouched:(UIBarButtonItem *)sender {
    [self.viewModel sortArrayByFavorites:^{
        [self.charactersTableView reloadData];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
