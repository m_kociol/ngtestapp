//
//  MainCharactersTableViewCell.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "MainCharactersTableViewCell.h"
#import "NumberConstants.h"
#import "Masonry.h"

@implementation MainCharactersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self createCellView];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - loadView

- (void)createCellView {
    // ImageView configuration
    self.thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ThumbnailSize, ThumbnailSize)];
    self.thumbnail.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.thumbnail];
    [self.thumbnail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(ThumbnailTopOffset);
        make.left.equalTo(self.contentView.mas_left).with.offset(ThumbnailLeftOffset);
        make.height.mas_equalTo(ThumbnailSize);
        make.width.mas_equalTo(ThumbnailSize);
    }];
    
    // AbstractLabel configuration
    self.abstractLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.abstractLabel.numberOfLines = 0;
    [self.abstractLabel sizeToFit];
    [self.contentView addSubview:self.abstractLabel];
    [self.abstractLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(AbstractLeftOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(AbstractRightOffset);
        make.top.equalTo(self.thumbnail.mas_bottom).with.offset(AbstractTopOffset);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(AbstractBottomOffset);
    }];
    
    // TitleLabel addSubview
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.titleLabel];
    
    // FavoriteButton configuration
    self.favoriteButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:self.favoriteButton];
    [self.favoriteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).with.offset(FavoriteRightOffset);
        make.centerY.equalTo(self.thumbnail);
        make.height.mas_equalTo(FavoriteSize);
        make.width.mas_equalTo(FavoriteSize);
    }];
    
    // TitleLabel configuration
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.thumbnail.mas_right).with.offset(TitleLeftOffset);
        make.right.equalTo(self.favoriteButton.mas_left).with.offset(TitleRightOffset);
        make.height.mas_equalTo(TitleHeight);
        make.centerY.equalTo(self.thumbnail);
    }];
}

+ (NSString *)reuseIdentifier {
    return @"mainCharactersCell";
}

@end
