//
//  StringConstants.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringConstants : NSObject

extern NSString *const MainCharactersTableViewCellNibName;

@end
