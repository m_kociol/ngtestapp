//
//  DetailViewController.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "DetailViewController.h"
#import "NumberConstants.h"
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CharacterDetail.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"navigation.detail.title", nil);
    [self createView];
    [self displayCharacterDetails];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ViewCreator

- (void)createView {
    CGFloat viewHeight;
    if (self.view.bounds.size.height < self.view.bounds.size.width) {
        viewHeight = self.view.bounds.size.width;
    } else {
        viewHeight = self.view.bounds.size.height;
    }
    
    if (self.characterDetail.isFavorite == YES) {
        self.view.backgroundColor = [UIColor yellowColor];
    } else {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    
    // ScrollView configuration
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.view.mas_top);
        make.height.mas_equalTo(viewHeight);
        make.bottom.equalTo(self.view.mas_bottom);
    }];

    // ContentView configuration
    self.contentView = [[UIView alloc] initWithFrame:CGRectZero];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left);
        make.right.equalTo(self.view.mas_right);
        make.top.equalTo(self.scrollView.mas_top);
        make.height.mas_equalTo(viewHeight - NavigationBarHeight);
        make.bottom.equalTo(self.scrollView.mas_bottom);
    }];

    
    // ImageView configuration
    self.thumbnail = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ThumbnailSize, ThumbnailSize)];
    self.thumbnail.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.thumbnail];
    [self.thumbnail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).with.offset(PictureTopOffset);
        make.centerX.equalTo(self.contentView);
        make.height.mas_equalTo(PictureSize);
        make.width.mas_equalTo(PictureSize);
    }];
    
    // TitleLabel configuration
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(TitleLeftOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(TitleRightOffset);
        make.height.mas_equalTo(TitleHeight);
        make.top.equalTo(self.thumbnail.mas_bottom).with.offset(TitleTopOffset);
    }];
    
    // FavoriteButton configuration
    self.showSiteButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.showSiteButton setTitle:NSLocalizedString(@"detail.show.safari.button.title", nil) forState:UIControlStateNormal];
    self.showSiteButton.layer.borderColor = [UIColor blueColor].CGColor;
    self.showSiteButton.layer.borderWidth = BorderWidth;
    self.showSiteButton.layer.cornerRadius = CornerRadius;
    [self.showSiteButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.showSiteButton addTarget:self
                            action:@selector(showSafari:)
                  forControlEvents:UIControlEventTouchUpInside];

    [self.contentView addSubview:self.showSiteButton];
    [self.showSiteButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).with.offset(ShowButtonRightOffset);
        make.left.equalTo(self.contentView.mas_left).with.offset(ShowButtonLeftOffset);
        make.bottom.equalTo(self.contentView.mas_bottom).with.offset(ShowButtonBottomOffset);
        make.height.mas_equalTo(ShowButtonHeigth);
        make.centerX.equalTo(self.contentView);
    }];
    
    // AbstractLabel configuration
    self.abstractLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.abstractLabel.numberOfLines = 0;
    [self.abstractLabel sizeToFit];
    self.abstractLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.abstractLabel];
    [self.abstractLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).with.offset(AbstractLeftOffset);
        make.right.equalTo(self.contentView.mas_right).with.offset(AbstractRightOffset);
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(AbstractTopOffset);
        make.bottom.equalTo(self.showSiteButton.mas_top).with.offset(AbstractBottomOffset);
    }];
}

#pragma mark - Private

- (void)displayCharacterDetails {
    [self.thumbnail sd_setImageWithURL:[NSURL URLWithString:self.characterDetail.characterDetailThumbnail] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    self.titleLabel.text = self.characterDetail.characterDetailTitle;
    self.abstractLabel.text = self.characterDetail.characterDetailAbstract;
}

#pragma mark - Button

- (void)showSafari:(UIButton*)sender {
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@", [self.baseURL absoluteString], self.characterDetail.characterDetailUrl];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringUrl]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
