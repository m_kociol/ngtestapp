//
//  MainViewModel.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>

@class APIClient;
@class MainCharactersTableViewCell;
@class Character;

@protocol LongPressureDelegate <NSObject>

- (void)expandSelectedRowWithCell:(MainCharactersTableViewCell *)cell index:(NSInteger)index;

@end

@interface MainViewModel : NSObject <UITableViewDataSource>

@property (nonatomic, strong) APIClient *apiClient;
@property (nonatomic, strong) Character *character;
@property (nonatomic, weak) NSObject<LongPressureDelegate> *delegate;
@property (nonatomic, strong) UITableView *tableView;

- (void)sortArrayByFavorites:(void (^) ())completion;
- (RACSignal *)getCharacters;

@end
