//
//  ViewControllerAssembly.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class ViewModelAssembly;
@class MainViewController;
@class DetailViewController;

@interface ViewControllerAssembly : TyphoonAssembly

@property (nonatomic, strong) ViewModelAssembly *viewModelAssembly;

- (MainViewController *)mainViewController;
- (DetailViewController *)detailViewController;

@end
