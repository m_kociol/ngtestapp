//
//  CoreComponentsAssembly.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class APIClient;

@interface CoreComponentsAssembly : TyphoonAssembly

- (APIClient *)apiClient;

@end
