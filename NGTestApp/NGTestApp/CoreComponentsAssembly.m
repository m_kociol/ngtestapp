//
//  CoreComponentsAssembly.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "CoreComponentsAssembly.h"
#import "APIClient.h"

@implementation CoreComponentsAssembly

- (APIClient *)apiClient {
    return [TyphoonDefinition withClass:[APIClient class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(baseURL) with:TyphoonConfig(@"api_base")];
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id)configurer {
    return [TyphoonDefinition withConfigName:@"Config.json"];
}

@end
