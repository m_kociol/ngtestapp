//
//  APIClient.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "APIClient.h"
#import "RequestType.h"
#import <MTLJSONAdapter.h>
#import <MTLModel.h>
#import "Character.h"

@implementation APIClient


- (RACSignal *)getCharacters {
    NSString *endpoint = [NSString stringWithFormat:@"/api/v1/Articles/Top?expand=1&limit=75"];
    RACSignal *signal = [self makeRequestWithType:RequestTypeGET toEndpoint:endpoint withParams:nil body:nil forClass:[Character class]];
    return signal;
}

- (RACSignal *)rac_requestPath:(NSString *)path parameters:(id)parameters method:(NSString *)method {
    return [RACSignal createSignal:^(id<RACSubscriber> subscriber) {
        
        NSString *endpointPath = [NSString stringWithFormat:@"%@%@", self.baseURL, path];
        
        NSURL *url = [NSURL URLWithString:endpointPath];
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        request.HTTPMethod = method;
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                NSMutableDictionary *userInfo = [error.userInfo mutableCopy];
                NSError *errorWithRes = [NSError errorWithDomain:error.domain code:error.code userInfo:[userInfo copy]];
                [subscriber sendError:errorWithRes];
            } else {
                NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions
                                                  error:&error];
                [subscriber sendNext:RACTuplePack(jsonDictionary, nil)];
                [subscriber sendCompleted];
            }
        }];
        [task resume];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
}

- (RACSignal *)rac_GET:(NSString *)path parameters:(id)parameters {
    return [[self rac_requestPath:path parameters:parameters method:@"GET"]
            setNameWithFormat:@"%@ -rac_GET: %@, parameters: %@", self.class, path, parameters];
}

- (RACSignal *)makeRequestWithType:(RequestType)requestType toEndpoint:(NSString *)endpoint withParams:(NSDictionary *)params body:(NSDictionary *)body forClass:(Class)aClass {
    endpoint = [endpoint stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    RACSignal *signal;
    
    switch (requestType) {
        case RequestTypeGET:
            signal = [self rac_GET:endpoint parameters:params];
            break;
        default:
            break;
    }
    
    signal = [self preProcessResponseSignal:signal keyPath:nil];
    signal = [signal map:^id(id response) {
        id model = nil;
        NSError *error;
        if (aClass == nil) {
            return response;
        }
        if ([response isKindOfClass:[NSDictionary class]]) model = [MTLJSONAdapter modelOfClass:aClass fromJSONDictionary:response error:&error];
        else if ([response isKindOfClass:[NSArray class]]) model = [MTLJSONAdapter modelsOfClass:aClass fromJSONArray:response error:&error];
        return model;
    }];
    
    return signal;
}

- (RACSignal *)preProcessResponseSignal:(RACSignal *)signal keyPath:(NSString *)keyPath {
    return [self preProcessResponseSignal:signal keyPath:keyPath allowRefresh:YES];
}

- (RACSignal *)preProcessResponseSignal:(RACSignal *)signal keyPath:(NSString *)keyPath allowRefresh:(BOOL)allowRefresh {

    __block RACSignal *processedSignal;
    
    processedSignal = [[signal flattenMap:^RACStream *(RACTuple *tuple) {
        //We take response and uwrap it from RACTuple object
        RACTupleUnpack(id JSONResponse, NSURLResponse *response __unused) = tuple;
        NSNumber *responseStatusCode;
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            responseStatusCode = JSONResponse[@"code"];
        }
        
        NSDictionary *dataDict = keyPath ? [JSONResponse valueForKeyPath:keyPath] : JSONResponse;
        if (dataDict) {
            if (responseStatusCode.integerValue == 200 || responseStatusCode == nil) {
                //Finally send unwrapped data retrieved from server down through the signal
                return [RACSignal return:dataDict];
            } else {
                //Oops, something went wrong.Send error down the signal.
                NSError *error = [NSError errorWithDomain:@"com.response.error" code:responseStatusCode.integerValue userInfo:JSONResponse];
                return [RACSignal error:error];
            }
            //there was no response at all, send error down the signal
        } else {
            if (responseStatusCode.integerValue == 204 || responseStatusCode == nil) {
                return [RACSignal return:@"Success with empty response"];
            } else {
                NSError *error = [NSError errorWithDomain:@"com.no.response" code:42 userInfo:@{@"message": @"empty JSON response"}];
                return [RACSignal error:error];
            }
        }
        return nil;
        
    }] catch:^RACSignal *(NSError *error) {
        return [RACSignal error:error];
    }];
    return processedSignal;
}



@end
