//
//  CharacterDetail.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "CharacterDetail.h"

@implementation CharacterDetail

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"characterDetailTitle" : @"title",
             @"characterDetailUrl" : @"url",
             @"characterDetailAbstract" : @"abstract",
             @"characterDetailThumbnail" : @"thumbnail",
             };
}

@end
