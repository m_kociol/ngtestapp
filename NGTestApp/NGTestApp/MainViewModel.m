//
//  MainViewModel.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "MainViewModel.h"
#import "NumberConstants.h"
#import "MainCharactersTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "APIClient.h"
#import "Character.h"
#import "CharacterDetail.h"

@implementation MainViewModel

#pragma mark - API
- (RACSignal *)getCharacters {
    return [[self.apiClient getCharacters] doNext:^(Character *character) {
        NSError *error;
        character.characterDetail = [NSMutableArray new];
        for (NSDictionary *dict in character.items) {
            CharacterDetail *characterDetail = [MTLJSONAdapter modelOfClass:[CharacterDetail class] fromJSONDictionary:dict error:&error];
            characterDetail.isFavorite = NO;
            characterDetail.characterHeight = 0.0f;
            [character.characterDetail addObject:characterDetail];
        }
        self.character = character;
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        MainCharactersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[MainCharactersTableViewCell reuseIdentifier] forIndexPath:indexPath];
    NSArray *cellViews = [cell.contentView subviews];
    if (cellViews.count == 0) {
        [cell awakeFromNib];
    }
    
    [cell.favoriteButton addTarget:self
               action:@selector(favoriteTouched:)
     forControlEvents:UIControlEventTouchUpInside];
    
    if (self.character.characterDetail[indexPath.row].isFavorite == YES) {
        [cell.favoriteButton setBackgroundImage:[UIImage imageNamed:@"starFilled"] forState:UIControlStateNormal];
    } else {
        [cell.favoriteButton setBackgroundImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
    }
    
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]
                                                                initWithTarget:self action:@selector(handleLongPress:)];
    longPressGestureRecognizer.minimumPressDuration = 0.5;
    longPressGestureRecognizer.view.tag = indexPath.row;
    [cell addGestureRecognizer:longPressGestureRecognizer];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.thumbnail sd_setImageWithURL:[NSURL URLWithString:self.character.characterDetail[indexPath.row].characterDetailThumbnail] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.titleLabel.text = self.character.characterDetail[indexPath.row].characterDetailTitle;
    cell.abstractLabel.text = self.character.characterDetail[indexPath.row].characterDetailAbstract;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.character.characterDetail.count;
}

- (void)favoriteTouched:(UIButton*)sender {
    NSLog(@"you clicked on button ");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (self.character.characterDetail[indexPath.row].isFavorite == YES) {
        self.character.characterDetail[indexPath.row].isFavorite = NO;
        [sender setBackgroundImage:[UIImage imageNamed:@"starEmpty"] forState:UIControlStateNormal];
    } else {
        [sender setBackgroundImage:[UIImage imageNamed:@"starFilled"] forState:UIControlStateNormal];
        self.character.characterDetail[indexPath.row].isFavorite = YES;
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateEnded) {
        CGPoint location = [longPress locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];
        MainCharactersTableViewCell *cell = (MainCharactersTableViewCell *)longPress.view;
        [self.delegate expandSelectedRowWithCell:cell index:indexPath.row];
    }
}

- (void)sortArrayByFavorites:(void (^) ())completion{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isFavorite"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.character.characterDetail = (NSMutableArray *)[self.character.characterDetail sortedArrayUsingDescriptors:sortDescriptors];
    completion();
}

@end
