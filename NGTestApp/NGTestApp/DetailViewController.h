//
//  DetailViewController.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CharacterDetail;

@interface DetailViewController : UIViewController

@property (nonatomic, strong) CharacterDetail *characterDetail;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIImageView *thumbnail;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *abstractLabel;
@property (nonatomic, strong) UIButton *showSiteButton;
@property (nonatomic, strong) NSURL *baseURL;

@end
