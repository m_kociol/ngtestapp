//
//  ApplicationAssembly.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class ViewControllerAssembly;
@class AppDelegate;

@interface ApplicationAssembly : TyphoonAssembly

@property (nonatomic, strong) ViewControllerAssembly *viewControllerAssembly;

- (AppDelegate *)appDelegate;

@end
