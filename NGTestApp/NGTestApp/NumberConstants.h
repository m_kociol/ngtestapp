//
//  NumberConstants.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NumberConstants : NSObject

extern float const ThumbnailSize;
extern float const ThumbnailTopOffset;
extern float const ThumbnailLeftOffset;
extern float const CharacterCellHeight;
extern float const CharacterCellExtendedOffset;
extern float const TitleHeight;
extern float const TitleLeftOffset;
extern float const TitleRightOffset;
extern float const TitleTopOffset;
extern float const AbstractLeftOffset;
extern float const AbstractRightOffset;
extern float const AbstractTopOffset;
extern float const AbstractBottomOffset;
extern float const AbstractHeight;
extern float const FavoriteRightOffset;
extern float const FavoriteSize;
extern float const PictureSize;
extern float const PictureTopOffset;
extern float const ShowButtonLeftOffset;
extern float const ShowButtonRightOffset;
extern float const ShowButtonBottomOffset;
extern float const ShowButtonHeigth;
extern float const NavigationBarHeight;
extern float const BorderWidth;
extern float const CornerRadius;

@end
