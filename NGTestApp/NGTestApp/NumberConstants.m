//
//  NumberConstants.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 18.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "NumberConstants.h"

@implementation NumberConstants

float const ThumbnailSize = 50.0f;
float const ThumbnailTopOffset = 20.0f;
float const ThumbnailLeftOffset = 20.0f;
float const TitleHeight = 25.0f;
float const TitleLeftOffset = 20.0f;
float const TitleRightOffset = -20.0f;
float const TitleTopOffset = 20.0f;
float const AbstractLeftOffset = 20.0f;
float const AbstractRightOffset = -20.0f;
float const AbstractTopOffset = 20.0f;
float const AbstractHeight = 45.0f;
float const AbstractBottomOffset = -20.0f;
float const FavoriteRightOffset = -20.0f;
float const FavoriteSize = 35.0f;
float const CharacterCellHeight = 155.0f;
float const CharacterCellExtendedOffset = 20.0f;
float const PictureSize = 200.0f;
float const PictureTopOffset = 20.0f;
float const ShowButtonLeftOffset = 20.0f;
float const ShowButtonRightOffset = -20.0f;
float const ShowButtonBottomOffset = -20.0f;
float const ShowButtonHeigth = 50.0f;
float const NavigationBarHeight = 64.0f;
float const BorderWidth = 1.0f;
float const CornerRadius = 4.0f;

@end
