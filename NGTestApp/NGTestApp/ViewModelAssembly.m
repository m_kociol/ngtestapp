//
//  ViewModelAssembly.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "ViewModelAssembly.h"
#import "MainViewModel.h"
#import "CoreComponentsAssembly.h"

@implementation ViewModelAssembly

- (MainViewModel *)mainViewModel {
    return [TyphoonDefinition withClass:[MainViewModel class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(apiClient) with:[self.coreComponentsAssembly apiClient]];
    }];
}


@end
