//
//  ApplicationAssembly.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "ApplicationAssembly.h"
#import "AppDelegate.h"
#import "ViewControllerAssembly.h"

@implementation ApplicationAssembly

- (AppDelegate *)appDelegate {
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(window) with:[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds]];
        [definition injectProperty:@selector(mainViewController) with:[self.viewControllerAssembly mainViewController]];
    }];
}


@end
