//
//  Character.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 20.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "Character.h"

@implementation Character

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"basePath" : @"basepath",
             @"items" : @"items",
             };
}


@end
