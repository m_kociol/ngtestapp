//
//  ViewControllerAssembly.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import "ViewControllerAssembly.h"
#import "ViewModelAssembly.h"
#import "MainViewController.h"
#import "ViewModelAssembly.h"
#import "DetailViewController.h"

@implementation ViewControllerAssembly

- (MainViewController *)mainViewController {
    return [TyphoonDefinition withClass:[MainViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition injectProperty:@selector(viewModel) with:[self.viewModelAssembly mainViewModel]];
        [definition injectProperty:@selector(baseURL) with:TyphoonConfig(@"api_base")];
    }];
}

- (DetailViewController *)detailViewController {
    return [TyphoonDefinition withClass:[MainViewController class] configuration:^(TyphoonDefinition *definition) {
        
    }];
}

- (id)configurer {
    return [TyphoonDefinition withConfigName:@"Config.json"];
}

@end
