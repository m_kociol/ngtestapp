//
//  MainViewController.h
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 17.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewModel;
@class MainCharactersTableViewCell;

@interface MainViewController : UIViewController

@property (nonatomic, strong) MainViewModel *viewModel;
@property (nonatomic, strong) NSURL *baseURL;

- (void)expandSelectedRowWithCell:(MainCharactersTableViewCell *)cell index:(NSInteger)index;

@end
