//
//  MappingObjects.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 22.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Expecta/Expecta.h>
#import <Mantle/MTLJSONAdapter.h>
#import <Mantle/MTLModel.h>
#import "Character.h"

@interface MappingObjects : XCTestCase

@property (nonatomic, strong) NSDictionary *parsedData;

@end

@implementation MappingObjects

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSString *mockDatabasePath = [[NSBundle mainBundle] pathForResource:@"CharacterAPIResponseMock" ofType:@"json"];
    NSError *error;
    NSString *jsonString = [NSString stringWithContentsOfFile:mockDatabasePath encoding:NSUTF8StringEncoding error:&error];
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    self.parsedData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIfCharacterExistsAfterMapping {
    NSError *error;
    Character *character = [MTLJSONAdapter modelOfClass:[Character class] fromJSONDictionary:self.parsedData error:&error];
    expect(character.basePath).notTo.equal(nil);
}

- (void)testIfCharacterIsCorrectlyMapped {
    NSError *error;
    Character *character = [MTLJSONAdapter modelOfClass:[Character class] fromJSONDictionary:self.parsedData error:&error];
    expect(character.basePath).equal(@"http://gameofthrones.wikia.com");
}

- (void)testIfCharacterDetailExistsAfterMapping {
    NSError *error;
    Character *character = [MTLJSONAdapter modelOfClass:[Character class] fromJSONDictionary:self.parsedData error:&error];
    character.characterDetail = [NSMutableArray new];
    for (NSDictionary *dict in character.items) {
        CharacterDetail *characterDetail = [MTLJSONAdapter modelOfClass:[CharacterDetail class] fromJSONDictionary:dict error:&error];
        characterDetail.isFavorite = NO;
        characterDetail.characterHeight = 0.0f;
        [character.characterDetail addObject:characterDetail];
    }

    expect(character.characterDetail).notTo.equal(nil);
}

- (void)testIfCharacterDetailAbstractMapping {
    NSError *error;
    Character *character = [MTLJSONAdapter modelOfClass:[Character class] fromJSONDictionary:self.parsedData error:&error];
    character.characterDetail = [NSMutableArray new];
    for (NSDictionary *dict in character.items) {
        CharacterDetail *characterDetail = [MTLJSONAdapter modelOfClass:[CharacterDetail class] fromJSONDictionary:dict error:&error];
        characterDetail.isFavorite = NO;
        characterDetail.characterHeight = 0.0f;
        [character.characterDetail addObject:characterDetail];
    }
    
    expect(character.characterDetail[0].characterDetailAbstract).equal(@"King Jon Snow is a major character in the first, second, third, fourth, fifth, sixth, and...");
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
