//
//  MainCharacterTableViewCellTest.m
//  NGTestApp
//
//  Created by Maksymilian Kotlowski on 22.11.2016.
//  Copyright © 2016 Maksymilian Kotlowski. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Expecta/Expecta.h>
#import "CharacterDetail.h"
#import "MainCharactersTableViewCell.h"

@interface MainCharacterTableViewCellTest : XCTestCase

@property (nonatomic, strong) MainCharactersTableViewCell *cell;
@property (nonatomic, strong) CharacterDetail *characterDetail;

@end

@implementation MainCharacterTableViewCellTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.cell = [[MainCharactersTableViewCell alloc] init];
    [self.cell awakeFromNib];
    self.characterDetail = [[CharacterDetail alloc] init];
    self.characterDetail.characterDetailTitle = @"John";
    self.characterDetail.characterDetailAbstract = @"This is abstract string";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIfCellViewIsCorectlyConfiguredWithTitle {
    self.cell.titleLabel.text = self.characterDetail.characterDetailTitle;
    expect(self.cell.titleLabel.text).to.equal(@"John");
}

- (void)testIfCellViewIsCorectlyConfiguredThumbnailSize {
    expect(self.cell.thumbnail.bounds.size.height).to.equal(50);
}

- (void)testIfCellViewIsCorectlyConfiguredWithAbstract {
    self.cell.abstractLabel.text = self.characterDetail.characterDetailAbstract;
    expect(self.cell.abstractLabel.text).to.equal(@"This is abstract string");
}

- (void)testIfCellViewIsCorectlyConfiguredWithThumbnailColor {
    expect(self.cell.thumbnail.backgroundColor).to.equal([UIColor whiteColor]);
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
